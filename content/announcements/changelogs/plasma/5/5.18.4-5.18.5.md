---
aliases:
- /announcements/plasma-5.18.4-5.18.5-changelog
hidden: true
plasma: true
title: Plasma 5.18.5 Complete Changelog
version: 5.18.5
---

<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/bluedevil/0eb6da9c155648db471dd6af0b5510a6d52fd05c'>Commit.</a> </li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Make it compile against qt5.15. SkipEmptyParts is part of Qt::. <a href='https://commits.kde.org/discover/6ef9094f6db1ee6e00d96c04ce7e8aa212fdfdab'>Commit.</a> </li>
<li>Flatpak: Don't issue commands when cancelled. <a href='https://commits.kde.org/discover/498e6d6ec51dd7faaf65dd349cd4435f5d940f19'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404622'>#404622</a></li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Disable automatic cookie injection. <a href='https://commits.kde.org/drkonqi/abb0115bf7bee74c5be19191fc6bc0e1c84d5326'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419646'>#419646</a></li>
<li>Don't leak DrKonqi dialog / fix crash on wayland. <a href='https://commits.kde.org/drkonqi/a96d4878b4cd52237930112be899e6bbcaea3963'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28692'>D28692</a></li>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/drkonqi/054ff058ad683e0b4c5a2d64806751af169fae69'>Commit.</a> </li>
<li>Be a bit more verbose on protocol exceptions. <a href='https://commits.kde.org/drkonqi/b2008aac754f292812181955300246f3f5a5a67e'>Commit.</a> </li>
<li>Avoid creating subwindow on internal page widget. <a href='https://commits.kde.org/drkonqi/262389b1e49ba12f6d8df8db71e350fb62698c3e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418755'>#418755</a>. Fixes bug <a href='https://bugs.kde.org/420035'>#420035</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28832'>D28832</a></li>
<li>Fix line rating for new format when function name is missing. <a href='https://commits.kde.org/drkonqi/113b91bfee13caa9590b1d029783db37a6783a52'>Commit.</a> See bug <a href='https://bugs.kde.org/418538'>#418538</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28027'>D28027</a></li>
<li>Further constrict line parsing of .so files. <a href='https://commits.kde.org/drkonqi/dd180bb4d057b7147c07beedd78d64b671498e40'>Commit.</a> See bug <a href='https://bugs.kde.org/418538'>#418538</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28026'>D28026</a></li>
</ul>


<h3><a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/kactivitymanagerd/7b85bc1d5d4688e1ac5da96b152d52e9a4008933'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/kdeplasma-addons/770381269cb9af140902f4aa78a12e2e95475eea'>Commit.</a> </li>
</ul>


<h3><a name='kgamma5' href='https://commits.kde.org/kgamma5'>Gamma Monitor Calibration Tool</a> </h3>
<ul id='ulkgamma5' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/kgamma5/2fadfe0ec4439a5047552dc12f022d5e91666e5c'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Adjust smb kcm size to be sufficient vis a vis headers. <a href='https://commits.kde.org/kinfocenter/f2c5e10d4af386be00719f7c75de992a80e18948'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419786'>#419786</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28678'>D28678</a></li>
<li>Strip non printable characters from opengl dri info. <a href='https://commits.kde.org/kinfocenter/a20ea703f65930207ecf89efe75cd0d3b12afe38'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27981'>D27981</a></li>
<li>Make opengl module support multiple dri devices. <a href='https://commits.kde.org/kinfocenter/72a09a3f2787fb32e9cde83f1f4fcf19e5ab54d2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417986'>#417986</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27980'>D27980</a></li>
</ul>


<h3><a name='kmenuedit' href='https://commits.kde.org/kmenuedit'>KMenuEdit</a> </h3>
<ul id='ulkmenuedit' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/kmenuedit/e632cd94a791ffcc4a4df93e7d066180bd2285b5'>Commit.</a> </li>
</ul>


<h3><a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a> </h3>
<ul id='ulksshaskpass' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/ksshaskpass/0ccf382d62f7eecfb3abb9dfd78ebd1bcbdbc60a'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/ksysguard/1909fbb8b5bbcc7b2bcfdd459c2f865ac5a0a76f'>Commit.</a> </li>
</ul>


<h3><a name='kwallet-pam' href='https://commits.kde.org/kwallet-pam'>kwallet-pam</a> </h3>
<ul id='ulkwallet-pam' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/kwallet-pam/45f99a1c5ff432b8acc1e6370733688623bfb872'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Make Compositor more verbose. <a href='https://commits.kde.org/kwin/f823be570c7274beeae4eb97aa2fa6869745fbf1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29261'>D29261</a></li>
<li>[scripting] Re-evaluate exclusions after switching between virtual desktops or activities. <a href='https://commits.kde.org/kwin/fd106de6509cca787ceccbf2962e6e2f27046639'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28827'>D28827</a></li>
<li>Avoid crash in KWin::DrmOutput::updateCursor. <a href='https://commits.kde.org/kwin/e8a1f8ecccb548af2f80b2619669da9dc135f175'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420077'>#420077</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28889'>D28889</a></li>
<li>[wayland] Fix teardown order. <a href='https://commits.kde.org/kwin/1a359d5e93947bd06276405c7b0ee0bb979cd1b6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28668'>D28668</a></li>
<li>[wayland] avoid potential crash when checking for window inhibitions on desktop change. <a href='https://commits.kde.org/kwin/d0875aa11707dd042e14f8723e1fd141452dca80'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420039'>#420039</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28858'>D28858</a></li>
<li>[kcmkwin] Make dialog non blocking. <a href='https://commits.kde.org/kwin/bee3afdd5194e0cf1fb72619b3d130514102e3cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419118'>#419118</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28293'>D28293</a></li>
</ul>


<h3><a name='kwrited' href='https://commits.kde.org/kwrited'>kwrited</a> </h3>
<ul id='ulkwrited' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/kwrited/15d8e6e9ed44d6873cbad4fe96e1dbb4ed0cdb4c'>Commit.</a> </li>
</ul>


<h3><a name='milou' href='https://commits.kde.org/milou'>Milou</a> </h3>
<ul id='ulmilou' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/milou/ae53eba546c5cba633ec03b8712678d63838d6a3'>Commit.</a> </li>
<li>Move QMimeData registration back to where it belongs. <a href='https://commits.kde.org/milou/92ecad19708db75fb35d912e172f026dc2d41606'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418109'>#418109</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28485'>D28485</a></li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>Detect Vivaldi based on binary name. <a href='https://commits.kde.org/plasma-browser-integration/97981b214ab62d1104e9bd0121966382be77356b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28893'>D28893</a></li>
<li>[Purpose Plugin] Detect cancelling the prompt more reliably. <a href='https://commits.kde.org/plasma-browser-integration/5a5369a0cb40d0bacda77063c069c2e34f320c28'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28719'>D28719</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Runners KCM] Notify history deletion. <a href='https://commits.kde.org/plasma-desktop/8fe03502660cdab99d2b0c592cf3667480fd4e77'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29410'>D29410</a></li>
<li>KCM Icons : Use KIconloader::emitChange() instead of deprecated newIconLoader(). <a href='https://commits.kde.org/plasma-desktop/9bbd2645626e574308c631a9e4adc312283df387'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420577'>#420577</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29285'>D29285</a></li>
<li>[Task Manager] Avoid crash with QStringBuilder. <a href='https://commits.kde.org/plasma-desktop/0eba5453b9d46d99dc6623b165b1be1c1659003c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420452'>#420452</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29224'>D29224</a></li>
<li>KCM LookAndFeel check if splash screen is provided before applying it. <a href='https://commits.kde.org/plasma-desktop/dacb3a8bdd206ec14a588bb25b37caa48c3b36aa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414417'>#414417</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28656'>D28656</a></li>
<li>KCM LookAndFeel check widgetStyle availability before applying it. <a href='https://commits.kde.org/plasma-desktop/17ef374a8cc469a9eefc516fc4885cbbce0c0afc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419552'>#419552</a>. Fixes bug <a href='https://bugs.kde.org/418698'>#418698</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28662'>D28662</a></li>
<li>[Folder View] Update popup dialog's hardcoded scrollbar width to fix faulty sizing. <a href='https://commits.kde.org/plasma-desktop/080e9dd6a4dc5e1985b11a91b4cebd945c6adfe8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/420385'>#420385</a></li>
<li>[Kickoff] Disable tabBar's mouseArea when searching. <a href='https://commits.kde.org/plasma-desktop/020c67fc8ba30240215f498eb8106635b05ecd04'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401861'>#401861</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28985'>D28985</a></li>
<li>Revert "Make accidental panel and panel widget deletion more difficult". <a href='https://commits.kde.org/plasma-desktop/0a1eb00b4bf76f57fcc0ae0bb08562dad7853d6b'>Commit.</a> </li>
<li>[KCMs/Standard Actions] Give KCMShell window a sane default size. <a href='https://commits.kde.org/plasma-desktop/8e0f525a96331102cdb3d20aadbc4cd8508e5146'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377653'>#377653</a>. Phabricator Code review <a href='https://phabricator.kde.org/D26695'>D26695</a></li>
<li>Make accidental panel and panel widget deletion more difficult. <a href='https://commits.kde.org/plasma-desktop/ba66d2eabe38bc357d799bff391cf5edbce139c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419853'>#419853</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28710'>D28710</a></li>
<li>KCM Colors fix apply button always disabled. <a href='https://commits.kde.org/plasma-desktop/443b028a1cb344604751963770105903b0a9e391'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418604'>#418604</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27944'>D27944</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. <a href='https://commits.kde.org/plasma-integration/53b409d7f04128e92460ccf9b9725982970fe44f'>Commit.</a> </li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Create StreamRestore channel if none exists. <a href='https://commits.kde.org/plasma-pa/5bdd1368957955c900d5a17bf6c890bb0920d356'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407397'>#407397</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28822'>D28822</a></li>
</ul>


<h3><a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. GIT_SILENT. <a href='https://commits.kde.org/plasma-sdk/75679cb2c389e20f3408bb14764e0b927bdc6bf9'>Commit.</a> </li>
</ul>


<h3><a name='plasma-thunderbolt' href='https://commits.kde.org/plasma-thunderbolt'>plasma-thunderbolt</a> </h3>
<ul id='ulplasma-thunderbolt' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. GIT_SILENT. <a href='https://commits.kde.org/plasma-thunderbolt/921f4d21b86ebd3d659b894a791ef9fd56bfe799'>Commit.</a> </li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a> </h3>
<ul id='ulplasma-vault' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. GIT_SILENT. <a href='https://commits.kde.org/plasma-vault/9177b871abbb7a4f5993ffcd271dbc244a5220e2'>Commit.</a> </li>
<li>Make sure we have saved network state before accessing it. <a href='https://commits.kde.org/plasma-vault/5920e79ebc695b4ce2937cbdac435e30389db96a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418262'>#418262</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[Notifications] Don't show do not disturb end date beyond 100 days. <a href='https://commits.kde.org/plasma-workspace/a5c9e000b9c9c814faacfecba7f6bf42eea64943'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28740'>D28740</a></li>
<li>[systemtray] Fix SNI icon not rendering. <a href='https://commits.kde.org/plasma-workspace/77975468dc108e87e0bc491ac2c2da7276a6b518'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419305'>#419305</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29386'>D29386</a></li>
<li>[KRunner] Watch config with KConfigWatcher. <a href='https://commits.kde.org/plasma-workspace/6db69ae095247cff9b91b6d8e4fe8b427ec83360'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29411'>D29411</a></li>
<li>[Notifications] Have checkIndex check if the index is valid. <a href='https://commits.kde.org/plasma-workspace/f2e34dbbd3b969890dd375a3f7fc6a59aad5582e'>Commit.</a> See bug <a href='https://bugs.kde.org/418347'>#418347</a>. Phabricator Code review <a href='https://phabricator.kde.org/D29297'>D29297</a></li>
<li>[Notifications] Fix typo. <a href='https://commits.kde.org/plasma-workspace/ed0a518861501f352a72e2b9c1d7d478b3333691'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D29214'>D29214</a></li>
<li>Remove too strict Qt/KF5 deprecation rule. GIT_SILENT. <a href='https://commits.kde.org/plasma-workspace/a5025212c8f3e16acf348ed78764ba88e9018658'>Commit.</a> </li>
<li>Remove too strict Qt/KF5 deprecation rule. GIT_SILENT. <a href='https://commits.kde.org/plasma-workspace/ca8e92a5602837161cf890ce9cfc3f3d02c4be8c'>Commit.</a> </li>
<li>Fix warning about connecting to a null sender, favoritesModel can be null. <a href='https://commits.kde.org/plasma-workspace/573100ed6f8bcaf9fef2d26b2d62661e6d021aef'>Commit.</a> </li>
<li>Fix variable assignment. <a href='https://commits.kde.org/plasma-workspace/70fa173d466a69ac85ceef778bced920f5996aaf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28905'>D28905</a></li>
<li>[Notifications] Don't reverse() Array. <a href='https://commits.kde.org/plasma-workspace/0fd991aa577a5e02b399175905b42a51126c3339'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28650'>D28650</a></li>
<li>Use the decoded url for launcherPosition. <a href='https://commits.kde.org/plasma-workspace/ac0124c48ce685db42ef0a99ce48e4b86621ed12'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418483'>#418483</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28632'>D28632</a></li>
<li>[Image Wallpaper] Fix thumbnail generation when model is reloaded in-flight. <a href='https://commits.kde.org/plasma-workspace/492301406a4656fbc6c9a1be0e77e68c5535bf93'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419234'>#419234</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28420'>D28420</a></li>
</ul>


<h3><a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a> </h3>
<ul id='ulplymouth-kcm' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. GIT_SILENT. <a href='https://commits.kde.org/plymouth-kcm/06db7d89f4bd5cb092a28d95a28fed9b84d1a96e'>Commit.</a> </li>
</ul>


<h3><a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. GIT_SILENT. <a href='https://commits.kde.org/polkit-kde-agent-1/d0df55fee005f447a3f3011a564a43090ef0dc6b'>Commit.</a> </li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. GIT_SILENT. <a href='https://commits.kde.org/sddm-kcm/a57bf5eda00905e65d153b8e978c9869cb55dcaf'>Commit.</a> </li>
</ul>


<h3><a name='user-manager' href='https://commits.kde.org/user-manager'>User Manager</a> </h3>
<ul id='uluser-manager' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. GIT_SILENT. <a href='https://commits.kde.org/user-manager/40763c1488053a40184a8cd6fb819ccbc5ad0a7c'>Commit.</a> </li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Remove too strict Qt/KF5 deprecation rule. GIT_SILENT. <a href='https://commits.kde.org/xdg-desktop-portal-kde/45493fcf612dd33b1169ad5d6486feb2606527c6'>Commit.</a> </li>
<li>ScreenSharing: close dialogs when session is closed. <a href='https://commits.kde.org/xdg-desktop-portal-kde/556f26ac2db57fc6087a958f8eb75c1bda4592c0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28618'>D28618</a></li>
</ul>