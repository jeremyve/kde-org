------------------------------------------------------------------------
r837118 | rjarosz | 2008-07-23 22:31:51 +0200 (Wed, 23 Jul 2008) | 6 lines

Backport commit 837114.
Protocols aren't always in Plugins group even if they are loaded because they are loaded automatically so we should ignore them.

CCBUG: 167113


------------------------------------------------------------------------
r838044 | aacid | 2008-07-26 16:25:19 +0200 (Sat, 26 Jul 2008) | 2 lines

Backport r838043 kopete/trunk/KDE/kdenetwork/kopete/kopete/config/avdevice/avdeviceconfig_videodevice.ui: Add a comment as "Standard" is not distinctive enough to know what it speaks about and it's easy to collide with other translations

------------------------------------------------------------------------
r838186 | scripty | 2008-07-27 06:41:52 +0200 (Sun, 27 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r838590 | rjarosz | 2008-07-28 11:30:11 +0200 (Mon, 28 Jul 2008) | 6 lines

Backport commit 838589.
Fix contact list background color.

CCBUG: 166053


------------------------------------------------------------------------
r838850 | rjarosz | 2008-07-28 23:52:43 +0200 (Mon, 28 Jul 2008) | 6 lines

Backport commit 838844.
Fix own avatar mess.
All myself contacts have the same metaContact so we can't take
photo from metaContact but we have to take it directly from myself contact.


------------------------------------------------------------------------
r839325 | uwolfer | 2008-07-29 22:05:50 +0200 (Tue, 29 Jul 2008) | 5 lines

Backport:
SVN commit 839324 by uwolfer:

Show tooltips in remote desktop dock widget (useful when there is to few place to display full text).
CCBUG:167638
------------------------------------------------------------------------
r839421 | mattr | 2008-07-30 03:44:42 +0200 (Wed, 30 Jul 2008) | 1 line

Bump version - 0.60.1 will be with KDE 4.1.1
------------------------------------------------------------------------
r839455 | scripty | 2008-07-30 07:34:37 +0200 (Wed, 30 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r840296 | mueller | 2008-07-31 22:44:31 +0200 (Thu, 31 Jul 2008) | 1 line

fix linking
------------------------------------------------------------------------
r840529 | uwolfer | 2008-08-01 12:17:35 +0200 (Fri, 01 Aug 2008) | 6 lines

Backport:
SVN commit 840408 by lappelhans:

Add trailing slash even if the newtransferdialog is not shown

CCBUG:163744
------------------------------------------------------------------------
r840688 | rjarosz | 2008-08-01 16:22:13 +0200 (Fri, 01 Aug 2008) | 5 lines

Backport fix for bug 167987: kopete doesn't restore status title after reconnecting

CCBUG: 167987


------------------------------------------------------------------------
r840790 | rjarosz | 2008-08-01 22:30:41 +0200 (Fri, 01 Aug 2008) | 6 lines

Backport commit 840788.
Delete KopeteWindow if we are shutting down otherwise KGlobal::deref() isn't called.

CCBUG: 167875


------------------------------------------------------------------------
r841061 | uwolfer | 2008-08-02 16:26:22 +0200 (Sat, 02 Aug 2008) | 8 lines

Backport:
SVN commit 841060 by uwolfer:

Correctely respect key modifiers in VNC.
Patch by Guillaume Pothier, thanks a lot!
CCBUG:168015
CCBUG:162865
(#162865: fixes KDE 4 issues, other issues listed there are already fixed in KDE 4)
------------------------------------------------------------------------
r843075 | uwolfer | 2008-08-06 13:40:42 +0200 (Wed, 06 Aug 2008) | 6 lines

Backport:
SVN commit 843074 by uwolfer:

Fixes situations were the Alt key is not released.
Patch by Guillaume Pothier, thanks.
CCBUG:168015
------------------------------------------------------------------------
r843367 | scripty | 2008-08-07 06:48:30 +0200 (Thu, 07 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r843590 | mlaurent | 2008-08-07 13:41:12 +0200 (Thu, 07 Aug 2008) | 3 lines

Backport:
show doc in khelpcenter

------------------------------------------------------------------------
r847819 | uwolfer | 2008-08-16 14:05:51 +0200 (Sat, 16 Aug 2008) | 5 lines

Backport:
SVN commit 844118 by uwolfer:

Fix handling of shift+tab.
Patch by Guillaume Pothier, thanks.
------------------------------------------------------------------------
r847821 | uwolfer | 2008-08-16 14:08:04 +0200 (Sat, 16 Aug 2008) | 9 lines

Backport:
SVN commit 847152 by uwolfer:

Fix some fullscreen toolbar issues:
* the toolbar autohides even if the mouse is inside it
* the toolbar sometimes does not appear (race condition)

Based on a patch by Guillaume Pothier, thanks. Minor improvements done by me.
CCBUG:168932
------------------------------------------------------------------------
r847958 | uwolfer | 2008-08-16 16:01:34 +0200 (Sat, 16 Aug 2008) | 6 lines

Backport:
SVN commit 847957 by uwolfer:

Improve artificats issue in scaled mode a lot.
Patch by Guillaume Pothier, thanks a lot!
CCBUG:168880
------------------------------------------------------------------------
r847963 | uwolfer | 2008-08-16 16:11:21 +0200 (Sat, 16 Aug 2008) | 6 lines

Backport:
SVN commit 847962 by uwolfer:

Send correct mouse wheel events when scaling is enabled.
Patch by Guillaume Pothier, thanks.
BUG:168919
------------------------------------------------------------------------
r852794 | sengels | 2008-08-26 18:12:09 +0200 (Tue, 26 Aug 2008) | 1 line

windows compile fixes
------------------------------------------------------------------------
r852976 | ereslibre | 2008-08-27 00:45:05 +0200 (Wed, 27 Aug 2008) | 2 lines

Backport commit 852972. We will find a solution for this...

------------------------------------------------------------------------
r853066 | scripty | 2008-08-27 08:05:19 +0200 (Wed, 27 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r853328 | rjarosz | 2008-08-27 16:58:20 +0200 (Wed, 27 Aug 2008) | 6 lines

Backport commit 846257.
Fix bug 168978 chat window is raised across different desktops when message queue is off.

CCBUG:168978


------------------------------------------------------------------------
