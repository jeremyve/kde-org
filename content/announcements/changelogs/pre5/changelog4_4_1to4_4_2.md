---
aliases:
- ../changelog4_4_1to4_4_2
hidden: true
title: KDE 4.4.2 Changelog
---

<h2>Changes in KDE 4.4.2</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_4_2/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdecore">kdecore</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Resolve mimetype aliases by default; fixes nspluginscan creating mimetypes that already exist (as aliases), like audio/mp3. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=197346">197346</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1097954&amp;view=rev">1097954</a>. </li>
        <li class="bugfix ">Detect aliases that are also known as real types (can happen with 3rd party xml files), to prevent inconsistencies when editing mimetypes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=218735">218735</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1097945&amp;view=rev">1097945</a>. </li>
        <li class="bugfix ">Fix the disabling of debug output when compiling apps in release mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=227089">227089</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1105396&amp;view=rev">1105396</a>. </li>
      </ul>
      </div>
      <h4><a name="plasma">plasma</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make extender not crash when moved. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=229566">229566</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=224952">224952</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1100005&amp;view=rev">1100005</a>. </li>
      </ul>
      </div>
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix a bug causing crashes in many different programs,
            including Plasma, KRunner, and KWin due to improperly locked pixmap cache deletion.
         Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=182026">182026</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1106080&amp;view=rev">1106080</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_4_2/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Prevent accidental execution of commands in the terminal when changing the directory. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=161637">161637</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1103209&amp;view=rev">1103209</a>. </li>
        <li class="bugfix ">Fixed possible crashes in the Information Panel and tooltips, when the receiving of meta data takes too long. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=224848">224848</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=222324">222324</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1097312&amp;view=rev">1097312</a>. </li>
      </ul>
      </div>
      <h4><a name="konqueror">konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash when closing a tab showing a PDF document (and the sidebar had focus). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=213876">213876</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1104130&amp;view=rev">1104130</a>. </li>
      </ul>
      </div>
      <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Set the correct action in screen edge configuration module on load. See SVN commit <a href="http://websvn.kde.org/?rev=1101217&amp;view=rev">1101217</a>. </li>
        <li class="bugfix ">Fix flickering when clicking on a window in Desktop Grid. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=229741">229741</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1103104&amp;view=rev">1103104</a>. </li>
        <li class="bugfix ">Windows are rearranged in Desktop Grid and Present Windows if the geometry of a window changes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=228829">228829</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1103206&amp;view=rev">1103206</a>. </li>
        <li class="bugfix ">Correctly handle mouse clicks in classic Alt+Tab list when the selected window is shown additionally. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=226877">226877</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1103492&amp;view=rev">1103492</a>. </li>
        <li class="bugfix ">Prevent modal dialogs to be shown twice during Alt+Tab. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=230807">230807</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1105473&amp;view=rev">1105473</a>. </li>
        <li class="bugfix ">Show windows on all desktops in Cube if windows hover above the Cube. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203086">203086</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1105593&amp;view=rev">1105593</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_4_2/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org/" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash when invoking print preview and then opening the annotation tools. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=231123">231123</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1104495&amp;view=rev">1104495</a>. </li>
        <li class="bugfix ">Fix crash in certain documents when rendering with high zoom. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=230282">230282</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1106764&amp;view=rev">1106764</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeplasma-addons"><a name="kdeplasma-addons">kdeplasma-addons</a><span class="allsvnchanges"> [ <a href="4_4_2/kdeplasma-addons.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="microblog applet">microblog applet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Timeline now displays correct times. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=194302">194302</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1096698&amp;view=rev">1096698</a>. </li>
      </ul>
      </div>
      <h4><a name="audioplayercontrol runner">audioplayercontrol runner</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Prevent the runner from causing the KRunner interface to freeze. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203668">203668</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1106771&amp;view=rev">1106771</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_4_2/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix display alarm whose text is generated by a command and which has an audio file, being converted into an audio-only alarm when reloaded.. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=230819">230819</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1104509&amp;view=rev">1104509</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_4_2/kdemultimedia.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://developer.kde.org/~wheeler/juk.html" name="juk">JuK</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix a possible crash when loading unrecognized media files. See SVN commit <a href="http://websvn.kde.org/?rev=1106103&amp;view=rev">1106103</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_4_2/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg/" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix misleading error message after importing keys from keyserver. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=231079">231079</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1105567&amp;view=rev">1105567</a>. </li>
        <li class="bugfix ">properly handle 8bit messages from "gpg --decrypt". Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=231154">231154</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1106908&amp;view=rev">1106908</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebindings"><a name="kdebindings">kdebindings</a><span class="allsvnchanges"> [ <a href="4_4_2/kdebindings.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://techbase.kde.org/Development/Languages/Python" name="pykde4">PyKDE4</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Added missing Nepomuk query classes. See SVN commit <a href="http://websvn.kde.org/?rev=1102304&amp;view=rev">1102304</a>. </li>
      </ul>
      </div>
    </div>