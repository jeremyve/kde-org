2005-05-27 15:24 +0000 [r418739]  nanulo

	* kbabel/common/kbprojectsettings.kcfg: fix context information
	  regexp CCMAIL: kbabel@kde.org

2005-05-27 15:48 +0000 [r418744]  nanulo

	* kbabel/catalogmanager/catalogmanager.cpp: move KBabel editor to
	  front

2005-05-29 07:40 +0000 [r419318]  nanulo

	* kbabel/kbabel/kbabelview.cpp, kbabel/kbabel/kbabelview2.cpp: -
	  don't autodelete spellchecker (backport) - don't store original
	  text in translatio memory with eoln (backport)

2005-05-30 17:58 +0000 [r419828]  weidendo

	* kcachegrind/kcachegrind/tabview.cpp: Backport: Fix initial tab
	  ordering (CalleeTab was hidden by default!)

2005-06-02 04:53 +0000 [r421077]  okellogg

	* umbrello/umbrello/association.cpp: Backport fix for bug 106632
	  from main trunk.

2005-06-02 20:15 +0000 [r421349]  okellogg

	* umbrello/umbrello/association.cpp: Backport fix for bug 106673
	  from main trunk.

2005-06-09 05:38 +0000 [r423638]  okellogg

	* umbrello/umbrello/associationwidget.cpp,
	  umbrello/umbrello/associationwidget.h: Backport unreopening of
	  bug 72016 from main trunk.

2005-06-09 17:54 +0000 [r423794]  okellogg

	* umbrello/umbrello/umlview.h,
	  umbrello/umbrello/associationwidget.cpp,
	  umbrello/umbrello/umlview.cpp: Backport fix for bug 106356.

2005-06-09 19:31 +0000 [r423811]  okellogg

	* umbrello/umbrello/uml.cpp, umbrello/umbrello/uml.h,
	  umbrello/ChangeLog: Backport fix for bug 103170.

2005-06-10 20:08 +0000 [r424114]  okellogg

	* umbrello/umbrello/model_utils.cpp,
	  umbrello/umbrello/umllistviewitem.cpp,
	  umbrello/umbrello/classifier.cpp,
	  umbrello/umbrello/classparser/cpptree2uml.cpp,
	  umbrello/umbrello/umllistview.cpp,
	  umbrello/umbrello/model_utils.h: Backport following fix from main
	  trunk: > SVN commit 424107 by okellogg: > Bring back input of
	  initial value at attributes and operation parameters.

2005-06-12 21:07 +0000 [r424726]  bram

	* kbabel/kbabel/kbcataloglistview.cpp: Backport of showing the
	  right ID.

2005-06-19 10:10 +0000 [r427047]  okellogg

	* umbrello/umbrello/umllistview.cpp, umbrello/ChangeLog,
	  umbrello/umbrello/umllistview.h: Backport fix for bug 107551 from
	  main trunk.

2005-06-27 12:53 +0000 [r429373]  jriddell

	* umbrello/VERSION: Update version for KDE 3.4.2

2005-06-27 15:18 +0000 [r429401]  binner

	* kdesdk.lsm: 3.4.2 preparations

2005-07-02 08:35 +0000 [r430777]  nanulo

	* kbabel/common/catalog.cpp: reinitialize plural form number for
	  each entry when searching CCMAIL: 102122-done@bugs.kde.org

2005-07-05 08:40 +0000 [r431787]  nanulo

	* kbabel/kbabel/main.cpp, kbabel/common/catalog.h: Lookup the
	  default project file only in the standard directories CCMAIL:
	  105053-done@bugs.kde.org

2005-07-05 09:07 +0000 [r431790]  nanulo

	* kbabel/kbabel/kbabelview.cpp: properly report problems while
	  saving CCMAIL: 106738-done@bugs.kde.org

2005-07-05 09:43 +0000 [r431802]  nanulo

	* kbabel/common/catalog.cpp: Applied patch to fix jumping to a
	  multiline message (Gaute Hvoslef Kvalnes) CCMAIL:
	  107797-done@bugs.kde.org

2005-07-05 10:12 +0000 [r431813-431812]  nanulo

	* kbabel/VERSION: bump

	* kbabel/filters/gettext/gettextimport.cpp: Report error on empty
	  files CCMAIL: 108279-done@bugs.kde.org

2005-07-05 11:13 +0000 [r431832]  nanulo

	* kbabel/common/kbprojectsettings.kcfg: fix default for plural
	  forms CCMAIL: 108555-done@bugs.kde.org

2005-07-07 05:47 +0000 [r432370]  okellogg

	* umbrello/umbrello/uml.cpp, umbrello/ChangeLog: Backport fix for
	  bug 107101 from main trunk.

2005-07-07 19:05 +0000 [r432543]  pletourn

	* kompare/komparepart/kompare_part.cpp: Remove misuse of KURL(const
	  QString&) BUG:97473

2005-07-08 03:51 +0000 [r432649]  okellogg

	* umbrello/umbrello/codegenerator.cpp: Backport fix for bug 108688
	  from main trunk.

2005-07-13 20:36 +0000 [r434368]  okellogg

	* umbrello/umbrello/codegenerators/cppsourcecodeoperation.cpp,
	  umbrello/ChangeLog, umbrello/THANKS,
	  umbrello/umbrello/codegenerators/cppheadercodeoperation.cpp:
	  BUG:97188 - updateContent(): Backport fix by Paulo Sehn.

2005-07-16 06:19 +0000 [r435054]  okellogg

	* umbrello/umbrello/dialogs/umloperationdialog.cpp,
	  umbrello/ChangeLog: Backport fix for bug 106183 from main trunk.

2005-07-18 15:17 +0000 [r435891]  nanulo

	* kbabel/kbabel/kbabelview.cpp: dont crash for incorrect number of
	  plural forms (backport)

