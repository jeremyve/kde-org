------------------------------------------------------------------------
r934916 | scripty | 2009-03-04 08:31:55 +0000 (Wed, 04 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r935443 | scripty | 2009-03-05 07:50:50 +0000 (Thu, 05 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r935487 | casanova | 2009-03-05 09:18:51 +0000 (Thu, 05 Mar 2009) | 2 lines

Only print Errors and fatal errors from ortp

------------------------------------------------------------------------
r935491 | casanova | 2009-03-05 09:27:34 +0000 (Thu, 05 Mar 2009) | 2 lines

Only print Errors and fatal errors from ortp (bad call in last commit)

------------------------------------------------------------------------
r935771 | scripty | 2009-03-06 07:43:50 +0000 (Fri, 06 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r936228 | scripty | 2009-03-07 07:52:17 +0000 (Sat, 07 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r937026 | lueck | 2009-03-08 21:46:57 +0000 (Sun, 08 Mar 2009) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r939615 | uwolfer | 2009-03-15 12:25:07 +0000 (Sun, 15 Mar 2009) | 3 lines

Fix a possible crash when a protocol support has not been built. Only valid for 4.2 branch.

BUG:179016
------------------------------------------------------------------------
r939619 | uwolfer | 2009-03-15 12:30:09 +0000 (Sun, 15 Mar 2009) | 4 lines

Backport:
SVN commit 939617 by uwolfer:

Better way to split additional arguments. Hopefully fixes comment #17 of bug #103934.
------------------------------------------------------------------------
r940101 | btsai | 2009-03-16 15:35:01 +0000 (Mon, 16 Mar 2009) | 5 lines

    Backport:
    SVN commit 939940 by btsai:

    Took two large code duplication that converted a kopete::message to a msn::message and pulled
it out into a function.
------------------------------------------------------------------------
r940102 | btsai | 2009-03-16 15:35:42 +0000 (Mon, 16 Mar 2009) | 6 lines

Backport:
SVN commit 939939 by btsai:

Currently the WLM plugin does not display the message until it receives acknowledgment that the
message got through. This patch changes the behavior so it displays the message immediately but
uses the message sending state to indicate whether or not the message has gone through.
------------------------------------------------------------------------
r940738 | scripty | 2009-03-18 08:18:40 +0000 (Wed, 18 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r940745 | mlaurent | 2009-03-18 08:27:08 +0000 (Wed, 18 Mar 2009) | 4 lines

Backport:
Fix warning about qaction+shortcut


------------------------------------------------------------------------
r940749 | mlaurent | 2009-03-18 08:35:17 +0000 (Wed, 18 Mar 2009) | 3 lines

Backport:
fix crash when interface return empty list

------------------------------------------------------------------------
r941253 | mlaurent | 2009-03-19 08:07:56 +0000 (Thu, 19 Mar 2009) | 2 lines

Backport: fix crash

------------------------------------------------------------------------
r941875 | mattr | 2009-03-20 16:01:34 +0000 (Fri, 20 Mar 2009) | 27 lines

Yahoo File Transfer fixups by John Groszko

Recieving works, but sending doesn't so far.

From the review request:

I'm trying to get Yahoo File Transfers working again, but I've gotten to
a point where I need input from someone with a fresh set of eyes or more
brains than me.

Receiving files works, but sending files does not. About 10k into the
transfer I see an unknown error code 16, and wireshark shows a bunch of
TCP Duplicate ACKs. The error looks like this in the debug log:

kopete(6494)/kopete (yahoo - raw protocol) SendFileTask::transmitData:
read: 1024  written:  1024
kopete(6494)/kopete (yahoo)
YahooAccount::slotFileTransferBytesProcessed: Transfer:  2  Bytes: 10240
kopete(6494)/kopete (yahoo - raw protocol) SendFileTask::transmitData:
kopete(6494)/kopete (yahoo - raw protocol) SendFileTask::connectFailed:
16 :  "an unknown/unexpected error has happened"

Some input/insight would be awesome, since I'd like to get this working
again...

Thanks for the patch John!
BUG: 159584
------------------------------------------------------------------------
r941876 | mattr | 2009-03-20 16:02:44 +0000 (Fri, 20 Mar 2009) | 3 lines

Make keypad 'Enter' also send messages by default.

Patch by Roman Spielerman. Thanks!
------------------------------------------------------------------------
r941877 | mattr | 2009-03-20 16:03:30 +0000 (Fri, 20 Mar 2009) | 5 lines

Fix the registration link for QQ.

Patch by Heath Matlock. Thanks!

BUG: 187270
------------------------------------------------------------------------
r942594 | scripty | 2009-03-22 07:43:12 +0000 (Sun, 22 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r942958 | mfuchs | 2009-03-23 00:01:53 +0000 (Mon, 23 Mar 2009) | 4 lines

Backport of r942957
Fix a typing-error that caused to use a wrong iterator. Now groups can be removed if they have contacts in them that are also elsewhere.
CCBUG:184050

------------------------------------------------------------------------
r943110 | pino | 2009-03-23 12:37:20 +0000 (Mon, 23 Mar 2009) | 2 lines

as agreed with Matt, remove the old msn plugin

------------------------------------------------------------------------
r943487 | rjarosz | 2009-03-23 22:34:42 +0000 (Mon, 23 Mar 2009) | 6 lines

Backport commit 943486.
Make AIM richtext messages well-formed so Qt can parse it,
now hopefully all richtext messages can be parsed correctly.
Some HTML escape fixes.


------------------------------------------------------------------------
r943677 | scripty | 2009-03-24 08:20:21 +0000 (Tue, 24 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r944102 | mattr | 2009-03-25 03:32:25 +0000 (Wed, 25 Mar 2009) | 1 line

bump version
------------------------------------------------------------------------
r944554 | dinkar | 2009-03-25 17:56:45 +0000 (Wed, 25 Mar 2009) | 8 lines

Backport of Commit 944550
Adding 'offline' and 'invisible' to status2Value.

This allows you to set status to offline and invisible from
dbus commands like setOnlineStatus

I am also backporting this (trivial) patch to the 4.2 branch, unless
someone objects
------------------------------------------------------------------------
