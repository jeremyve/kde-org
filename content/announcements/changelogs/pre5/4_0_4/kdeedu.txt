2008-04-25 09:19 +0000 [r800945-800944]  gladhorn

	* branches/KDE/4.0/kdeedu/libkdeedu/keduvocdocument/keduvockvtml2reader.cpp:
	  improve reading of documents created with kde 4.1 - sublesson
	  entries are read into the parent lesson

	* branches/KDE/4.0/kdeedu/parley/src/kvtlessonview.cpp: Fix crash
	  when clicking on empty space in lesson list. BUG:

2008-04-25 09:29 +0000 [r800948]  gladhorn

	* branches/KDE/4.0/kdeedu/parley/src/main.cpp: use
	  setQuitOnLastWindowClosed(false) to prevent parley from closing
	  when the practice is empty and the dialog box is closed

