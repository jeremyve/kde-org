------------------------------------------------------------------------
r1024617 | sebsauer | 2009-09-16 22:30:59 +0000 (Wed, 16 Sep 2009) | 5 lines

backport r1024616
Fix compile with Sun Studio 12 U1. Thanks tropikhajma.
BUG:207100


------------------------------------------------------------------------
r1025833 | sedwards | 2009-09-19 20:24:47 +0000 (Sat, 19 Sep 2009) | 2 lines

Changed calls to some of the old deprecated sip functions (sipConvertToInstance() etc) to the new proper way of doing things. (sipConvertToType() etc). PyKDE4 should now work with SIP 4.9 when it comes out.

------------------------------------------------------------------------
r1026823 | rdale | 2009-09-22 17:13:02 +0000 (Tue, 22 Sep 2009) | 4 lines

* Fix broken KDE::ConfigGroup.readEntry method. Thanks to Paulo Capriotti
  for reporting the bug


------------------------------------------------------------------------
r1029043 | sebsauer | 2009-09-28 20:41:16 +0000 (Mon, 28 Sep 2009) | 5 lines

backport r1029040
Fix crash if a python script does try to import kde3 dcop.
BUG:179823


------------------------------------------------------------------------
