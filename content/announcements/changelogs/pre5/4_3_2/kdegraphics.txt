------------------------------------------------------------------------
r1017643 | scripty | 2009-08-31 03:01:05 +0000 (Mon, 31 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1018123 | gateau | 2009-09-01 10:04:09 +0000 (Tue, 01 Sep 2009) | 1 line

Bumped version number
------------------------------------------------------------------------
r1019173 | scripty | 2009-09-03 04:10:13 +0000 (Thu, 03 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019361 | cgilles | 2009-09-03 11:42:32 +0000 (Thu, 03 Sep 2009) | 2 lines

backport commit #1019356

------------------------------------------------------------------------
r1019503 | lueck | 2009-09-03 18:51:18 +0000 (Thu, 03 Sep 2009) | 1 line

doc backport for 4.3
------------------------------------------------------------------------
r1019520 | lueck | 2009-09-03 19:42:01 +0000 (Thu, 03 Sep 2009) | 1 line

missed doc backport for 4.3
------------------------------------------------------------------------
r1019887 | lueck | 2009-09-04 18:19:15 +0000 (Fri, 04 Sep 2009) | 1 line

add to backport
------------------------------------------------------------------------
r1020513 | gateau | 2009-09-06 13:58:38 +0000 (Sun, 06 Sep 2009) | 3 lines

Ensure the hud is always fully visible.

BUG:201098
------------------------------------------------------------------------
r1020514 | gateau | 2009-09-06 13:58:43 +0000 (Sun, 06 Sep 2009) | 1 line

Updated
------------------------------------------------------------------------
r1020515 | gateau | 2009-09-06 13:58:48 +0000 (Sun, 06 Sep 2009) | 3 lines

Make sure currentUrl() returns the correct url, not the previous one.

BUG:185769
------------------------------------------------------------------------
r1020516 | gateau | 2009-09-06 13:58:53 +0000 (Sun, 06 Sep 2009) | 1 line

Updated
------------------------------------------------------------------------
r1020857 | cgilles | 2009-09-07 11:31:17 +0000 (Mon, 07 Sep 2009) | 2 lines

backport #990028 from trunk

------------------------------------------------------------------------
r1021415 | cgilles | 2009-09-09 06:31:12 +0000 (Wed, 09 Sep 2009) | 3 lines

backport commit #1021414
CCBUGS: 206712

------------------------------------------------------------------------
r1021674 | darioandres | 2009-09-09 19:43:58 +0000 (Wed, 09 Sep 2009) | 3 lines

- Fix a small memory leak


------------------------------------------------------------------------
r1022807 | cgilles | 2009-09-12 15:53:25 +0000 (Sat, 12 Sep 2009) | 2 lines

backport commit #1022795 from trunk

------------------------------------------------------------------------
r1023087 | aacid | 2009-09-13 21:14:04 +0000 (Sun, 13 Sep 2009) | 4 lines

Backport r1023085 | aacid | 2009-09-13 23:11:37 +0200 (Sun, 13 Sep 2009) | 3 lines

Remove special casing for saving local files where we don't use the temporary file, fixes saving a pdf file over itself

------------------------------------------------------------------------
r1023097 | aacid | 2009-09-13 22:05:27 +0000 (Sun, 13 Sep 2009) | 2 lines

Backport r1023096 okular/trunk/KDE/kdegraphics/okular/generators/dvi/generator_dvi.cpp: move usage of m_dviRenderer to below the if it checks if it exists

------------------------------------------------------------------------
r1024441 | scripty | 2009-09-16 16:12:45 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1028033 | gateau | 2009-09-25 15:35:32 +0000 (Fri, 25 Sep 2009) | 3 lines

Initialize spin boxes on start.

BUG:199946
------------------------------------------------------------------------
r1028034 | gateau | 2009-09-25 15:35:43 +0000 (Fri, 25 Sep 2009) | 1 line

Updated
------------------------------------------------------------------------
r1028113 | gateau | 2009-09-25 22:16:50 +0000 (Fri, 25 Sep 2009) | 1 line

Fix tricky bug where current url could be empty while viewing an image.
------------------------------------------------------------------------
r1028125 | gateau | 2009-09-25 22:53:19 +0000 (Fri, 25 Sep 2009) | 3 lines

Update previous/next action when dir has been fully listed

BUG:205468
------------------------------------------------------------------------
r1028126 | gateau | 2009-09-25 22:53:24 +0000 (Fri, 25 Sep 2009) | 1 line

Updated
------------------------------------------------------------------------
r1029177 | gateau | 2009-09-29 08:19:29 +0000 (Tue, 29 Sep 2009) | 3 lines

Do not crash if ptr is empty.

BUG:197563
------------------------------------------------------------------------
r1029178 | gateau | 2009-09-29 08:19:35 +0000 (Tue, 29 Sep 2009) | 3 lines

Backported fix for 197563

CCMAIL:andresbajotierra@gmail.com
------------------------------------------------------------------------
r1029872 | pino | 2009-09-30 23:26:07 +0000 (Wed, 30 Sep 2009) | 2 lines

bump version to 0.9.2

------------------------------------------------------------------------
