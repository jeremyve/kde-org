------------------------------------------------------------------------
r946721 | scripty | 2009-03-30 07:09:25 +0000 (Mon, 30 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r948730 | mlaurent | 2009-04-03 15:48:21 +0000 (Fri, 03 Apr 2009) | 3 lines

Backport:
Fix remove selected address

------------------------------------------------------------------------
r950520 | mlaurent | 2009-04-07 12:47:22 +0000 (Tue, 07 Apr 2009) | 4 lines

Backport:
Don't add [accept] etc when it's not an incidence as a freebusy mail


------------------------------------------------------------------------
r951769 | scripty | 2009-04-10 07:26:56 +0000 (Fri, 10 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r953497 | tmcguire | 2009-04-14 07:28:35 +0000 (Tue, 14 Apr 2009) | 8 lines

Backport r952440 by tmcguire from trunk to the 4.2 branch:

Don't add empty headers, even if they are mandatory, because that would cause an empty line in
the header, which was later parsed as a header/body separator.

Add a test for this.


------------------------------------------------------------------------
r954584 | winterz | 2009-04-15 23:31:33 +0000 (Wed, 15 Apr 2009) | 5 lines

backport SVN commit 954583 by winterz:

Fix "libkcal stores illegal negative DTSTART in ics / ical files".
Thanks for figuring out how to fix this Stephan.

------------------------------------------------------------------------
r955883 | momeny | 2009-04-18 18:34:03 +0000 (Sat, 18 Apr 2009) | 4 lines

Backport of 955873:
Fix tagging in GData::ModifyPost
A debug output that show user credentials removed from source.

------------------------------------------------------------------------
r955886 | momeny | 2009-04-18 18:39:20 +0000 (Sat, 18 Apr 2009) | 2 lines

SVN_SILENT Fix indentation, sorry :|

------------------------------------------------------------------------
r956204 | weilbach | 2009-04-19 14:03:56 +0000 (Sun, 19 Apr 2009) | 2 lines

Cleanup to use StoredTransferJob; Fix a missing setStatus in Blogger1 to get unit test running again.

------------------------------------------------------------------------
r957769 | smartins | 2009-04-22 20:39:37 +0000 (Wed, 22 Apr 2009) | 6 lines

Backport r955542 by smartins from trunk to the 4.2 branch:

Initialize these booleans. Thanks valgrind.

Solves the strange behaviour the rich text property was having in korganizer.

------------------------------------------------------------------------
r960523 | winterz | 2009-04-28 15:19:06 +0000 (Tue, 28 Apr 2009) | 2 lines

follow changes to writeICalDuration so it never writes the peculiar weeks duration

------------------------------------------------------------------------
r960542 | winterz | 2009-04-28 15:47:48 +0000 (Tue, 28 Apr 2009) | 4 lines

backport SVN commit 956417 by weilbach:

Fix a stupid typo.

------------------------------------------------------------------------
r960819 | woebbe | 2009-04-29 07:53:59 +0000 (Wed, 29 Apr 2009) | 3 lines

Don't leak KConfig in self().

Now only the factories are leaked :-(
------------------------------------------------------------------------
r960992 | smartins | 2009-04-29 12:39:13 +0000 (Wed, 29 Apr 2009) | 10 lines

Backport r955052 by smartins from trunk to the 4.2 branch:

Change the way resource local dir handles changes, this fixes some segfaults and avoids closing the calendar each time an incidence 
is modified.

Review: http://reviewboard.kde.org/r/516/
CCBUG: 180221
CCBUG: 187595


------------------------------------------------------------------------
