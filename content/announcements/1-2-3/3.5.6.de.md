---
aliases:
- ../announce-3.5.6
- ../announce-3.5.6.de
custom_about: true
custom_contact: true
date: '2007-01-25'
title: KDE-3.5.6-Pressemitteilung
---

<h3 align="center">
    Das KDE-Projekt gibt das f&uuml;nfte &Uuml;bersetzungs- und Wartungsrelease
	der f&uuml;hrenden, freien Arbeitsumgebung frei.
</h3>

<p align="justify">
  KDE 3.5.6 bringt &Uuml;bersetzungen in 65 verschiedene Sprachen und Verbesserungen
	der HTML-Rendering-Engine (KHTML) und anderen Anwendungen mit.
</p>

<p align="justify">
  Das <a href="http://www.kde.org/">KDE-Project</a>
  gab heute die sofortige Verf&uuml;gbarkeit von KDE 3.5.6, einem Wartungsrelease
  f&uuml;r die neuste Generation der fortschrittlichsten und umfangreichsten,
  <em>freien</em>, Arbeitsumgebung f&uuml;r GNU/Linux und andere unixoide Betriebssysteme bekannt.
  KDE unterst&uuml;tzt nun 65 Sprachen, was es f&uuml;r viel mehr Menschen zug&auml;nglich macht,
  als es die meiste kommerzielle Software ist. Zudem kann KDE um weitere Sprachen
  erg&auml;nzt werden, sofern sich Freiwillige finden, die an diesem Open-Source-Projekt
  mitarbeiten m&ouml;chten.
</p>

<p align="justify">
  Diese Version enth&auml;lt viele Fehlerkorrekturen f&uuml;r KHTML, <a href="http://kate.kde.org">Kate</a>, 
  Kicker, KSysguard und viele weitere Anwendungen. Bedeutende neue Eigenschaften sind
  die Unterst&uuml;tzung f&uuml;r den Fenstermanager Compiz mit Kicker, Sitzungsverwaltung f&uuml;r
  Browserunterfenster in <a href="http://akregator.kde.org">Akregator</a>, Nachrichtenvorlagen
  in <a href="kmail.kde.org">KMail</a>, und neue Men&uuml;s f&uuml;r Auswertungen in
  <a href="http://kontact.kde.org">Kontact</a>, die die Arbeit mit Terminen und Todo-Listen vereinfachen.
  Auch die &Uuml;bersetzung geht weiter, wobei sich die Vollst&auml;ndigkeit der
  <a href="http://l10n.kde.org/team-infos.php?teamcode=gl">galizischen</a> &Uuml;bersetzung mit 78% fast verdoppelt hat.
</p>

<p align="justify">
  Eine detailierte Liste der &Auml;nderungen seit
  <a href="http://www.kde.org/announcements/announce-3.5.5">KDE 3.5.5</a>
  vom 11. Oktober 2006, finden Sie in der 
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_5to3_5_6">Liste der &Auml;nderungen in KDE 3.5.6</a>.
</p>

<p align="justify">
  KDE 3.5.6 enth&auml;lt eine einfache Arbeitsumgebung, sowie f&uuml;nfzehn weitere
  Softwarepakete (PIM, Administration, Netzwerk, Lernprogramme, Dienstprogramme,
  Multimedia, Spiele, Graphik, Web-Entwicklung, und weitere). Die bereits mehrfach
  ausgezeichneten KDE-Werkzeuge und -Programme sind in <strong>65 Sprachen</strong>
  verf&uuml;gbar.
</p>

<h4>
  Distributionen, die KDE enthalten
</h4>
<p align="justify">
  Die meisten Linux-Distributionen und unixoiden Betriebssysteme werden die
  KDE-3.5.6-Pakete nicht sofort in ihre Distribution aufnehmen, sondern erst
  mit deren n&auml;chsten Ver&ouml;ffentlichung ausliefern. Werfen Sie einen Blick auf
  <a href="http://www.kde.org/download/distributions">diese Liste</a>,
  um zu sehen, welche Distributionen KDE enthalten.
</p>

<h4>
   Bin&auml;rpakete von KDE 3.5.6 installieren
</h4>
<p align="justify">
  <em>Paketersteller</em>.
  Einige Betriebssystemhersteller sind so freundlich, Bin&auml;rpakete von
  KDE 3.5.6 f&uuml;r einige ihrer Distributionen bereitzustellen. Wo dies nicht
  der Fall ist, werden die Pakete von freiwilligen Helfern erstellt.
  Einige dieser Pakete k&ouml;nnen kostenlos vom KDE-FTP-Server, unter
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.6/">http://download.kde.org</a>,
  heruntergeladen werden.
  Zus&auml;tzliche Bin&auml;rpakete, sowie Paket-Aktualisierungen werden im Laufe
  der n&auml;chsten Wochen verf&uuml;gbar sein.
</p>

<p align="justify">
  <a id="package_locations"><em>Verf&uuml;gbare Pakete</em></a>.
  F&uuml;r eine Liste der verf&uuml;gbaren Bin&auml;rpakete &uuml;ber die das KDE-Projekt
  informiert wurde, siehe die
  <a href="/info/3.5.6">Informationsseite zu KDE 3.5.6</a>.
</p>

<h4>
  KDE 3.5.6 kompilieren
</h4>
<p align="justify">
  <a id="source_code"></a><em>Quelltext</em>.
  Der vollst&auml;ndige Quelltext von KDE 3.5.6 kann
  <a href="http://download.kde.org/stable/3.5.6/src/">lostenlos heruntergeladen</a>
  werden. Hinweise zum Kompilieren und Installieren von KDE 3.5.6
  finden Sie auf der <a href="/info/3.5.6">Informationsseite zu KDE 3.5.6</a>.
</p>

<h4>
  KDE unterst&uuml;tzen
</h4>
<p align="justify">
KDE ist ein <a href="http://www.gnu.org/philosophy/free-sw.html">Freies Softwareprojekt</a>,
das nur aufgrund der Hilfe zahlreicher Freiwilliger existiert und w&auml;chst,
die ihre Zeit und Arbeit beisteuern. KDE ist immer auf der Suche
nach neuen Freiwilligen und Helfern, ganz egal ob sie Code schreiben,
Fehler beheben oder melden, Dokumentationen schreiben, &Uuml;bersetzungen
anfertigen, &Ouml;ffentlichkeitsarbeit leisten, Geld spenden usw.
Jede Hilfe ist herzlich willkommen, und wird gerne angenommen. Bitte lesen Sie die
Seite "<a href="/community/donations/">KDE unterst&uuml;tzen</a>" f&uuml;r weitere Informationen.</p>

<p align="justify">
Wir freuen uns, bald von Ihnen zu h&ouml;ren!
</p>

<h4>
  &Uuml;ber KDE
</h4>
<p align="justify">
  KDE ist ein mehrfach <a href="/community/awards/">ausgezeichnetes</a>, unabh&auml;ngiges Projekt
  <a href="/people/">hunderter</a> Entwickler, &Uuml;bersetzer, K&uuml;nstler und anderen
  Mitarbeitern auf der ganzen Welt, die &uuml;ber das Internet zusammenarbeiten um eine frei
  verf&uuml;gbare, stabile, an die pers&ouml;nlichen Bed&uuml;rfnisse anpassbare, flexible,
  komponentenbasierte, netzwerktransparente Arbeits- und B&uuml;roumgebung, sowie einen
  herausragende Plattform f&uuml;r Entwickler zu erstellen und zu verbreiten.
  </p>

<p align="justify">
  KDE bietet eine stabile, durchdachte Arbeitsumgebung die unter anderem
  einen modernen Webbrowser (<a href="http://konqueror.kde.org/">Konqueror</a>),
  eine PIM-Suite (<a href="http://kontact.org/">Kontact</a>),
  eine vollst&auml;ndige Office-Suite (<a href="http://www.koffice.org/">KOffice</a>),
  eine gro&szlig;e Auswahl an Netzwerkanwendungen und -Werkzeugen, sowie eine
  effiziente und intuitive Entwicklungsumgebung (<a href="http://www.kdevelop.org/">KDevelop</a>)
  enth&auml;lt.</p>

<p align="justify">
  KDE ist "der lebende Beweis", dass das "basar-artige" Entwicklungsmodell Freier
  Software erstklassige Technologien hervorbringen kann, welche gleichwertig,
  wenn nicht sogar &uuml;berlegen gegen&uuml;ber der komplexesten, kommerziellen Software sind.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Markenrechtshinweise</em>.
  KDE<sup>&#174;</sup> und das Logo der K Desktop Environment<sup>&#174;</sup>
  sind eingetragene Warenzeichen des KDE e.V.

  Linux ist ein eingetragenes Warenzeichen von Linus Torvalds.

  UNIX ist in den Vereinigten Staaten und anderen L&auml;ndern ein eingetragenes
  Warenzeichnen der Open Group.

  Alle anderen in dieser Pressemitteilungen erw&auml;hnten Warenzeichen und
  Copyright-Hinweise sind Eigentum der jeweiligen Besitzer.
  </font>
</p>

<hr />

<h4>Pressekontaktdaten</h4>
<table cellpadding="10" align="center"><tr valign="top">
<td>

<b>Afrika</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#109;a&#105;l&#116;o:&#105;&#110;fo-&#0097;&#0102;r&#105;&#99;a&#x40;k&#100;e.&#111;&#x72;g">info-africa kde.org</a><br />
</td>

<td>
<b>Asien und Indien</b><br />
  Pradeepto Bhattacharya<br/>
  A-4 Sonal Coop. Hsg. Society<br/>
  Plot-4, Sector-3,<br/>
  New Panvel,<br/>
  Maharashtra.<br/>
  India 410206<br/>
  Phone : +91-9821033168<br/>
<a href="ma&#0105;&#108;to&#00058;inf&#00111;-&#97;&#115;&#x69;a&#x40;kde.or&#x67;">info-asia kde.org</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europa</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="m&#x61;il&#0116;o&#x3a;i&#x6e;fo-&#00101;&#00117;rope&#64;k&#x64;&#x65;&#00046;o&#x72;&#00103;">info-europe kde.org</a>
</td>

<td>
<b>Nordamerica</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="&#109;ai&#x6c;&#x74;&#x6f;&#0058;i&#x6e;&#0102;o&#0045;no&#0114;t&#104;&#0097;m&#x65;&#x72;i&#x63;&#x61;&#x40;k&#x64;e&#46;&#0111;&#x72;&#x67;">info-northamerica kde.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Ozeanien</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="&#109;&#x61;&#x69;&#x6c;&#x74;o:&#105;&#x6e;fo&#45;&#x6f;c&#101;&#x61;&#110;ia&#064;kde&#00046;org">info-oceania kde.org</a><br />
</td>

<td>
<b>S&uuml;damerica</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="ma&#105;&#x6c;&#116;&#x6f;&#x3a;&#0105;&#110;&#102;&#x6f;-&#00115;&#111;ut&#104;&#97;&#x6d;e&#0114;&#x69;ca&#0064;&#107;d&#x65;.&#111;r&#x67;">info-southamerica kde.org</a><br />
</td>

</tr></table>