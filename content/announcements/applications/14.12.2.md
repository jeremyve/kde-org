---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE Ships Applications 14.12.2.
layout: application
title: KDE Ships KDE Applications 14.12.2
version: 14.12.2
---

{{% i18n_var "February 3, 2015. Today KDE released the second stability update for <a href='%[1]s'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../14.12.0" %}}

More than 20 recorded bugfixes include improvements to the anagram game Kanagram, Umbrello UML Modeller, the document viewer Okular and the virtual globe Marble.

{{% i18n_var "This release also includes Long Term Support versions of Plasma Workspaces %[1]s, KDE Development Platform %[2]s and the Kontact Suite %[2]s." "4.11.16" "4.14.5" %}}