---
title: KDE Platform 4.11 Delivers Better Performance
date: "2013-08-14"
hidden: true
---

August 14, 2013

KDE Platform 4 has been in feature freeze since the 4.9 release. This version consequently only includes a number of bugfixes and performance improvements.

The Nepomuk semantic storage and search engine received massive performance improvements, such as a set of read optimizations that make reading data up to six times faster. Indexing has become smarter, being split in two stages. The first stage retrieves general information (such as file type and name) immediately; additional information like media tags, author information, etc. is extracted in a second, somewhat slower stage. Metadata display on newly-created or freshly-downloaded content is now much faster. In addition, the Nepomuk developers improved the backup and restore system. Last but not least, Nepomuk can now also index a variety of document formats including ODF and docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption="Semantic features in action in Dolphin" width="600px">}}

Nepomuk’s optimized storage format and rewritten e-mail indexer require reindexing some of the hard drive’s content. Consequently the reindexing run will consume an unusual amount of computing performance for a certain period – depending on the amount of content that needs to be reindexed. An automatic conversion of the Nepomuk database will run on the first login.

There have been more minor fixes which <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>can be found in the git logs</a>.

#### Installing the KDE Development Platform

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

{{% i18n_var"KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/%[1]s'>http://download.kde.org</a> and can also be obtained on <a href='http://www.kde.org/download/cdrom.php'>CD-ROM</a> or with any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today." "4.11.0" %}}

##### Packages

{{% i18n_var"Some Linux/UNIX OS vendors have kindly provided binary packages of %[1]s for some versions of their distribution, and in other cases community volunteers have done so." "4.11.0" %}} <br />

##### Package Locations

{{% i18n_var"For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_%[1]s'>Community Wiki</a>." "4.11.0" %}}

{{% i18n_var"The complete source code for %[1]s may be <a href='/info/%[1]s.php'>freely downloaded</a>. Instructions on compiling and installing KDE software %[1]s are available from the <a href='/info/%[1]s.php#binary'>%[1]s Info Page</a>." "4.11.0" %}}

#### System Requirements

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Also Announced Today:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Gearing up for long term maintenance, Plasma Workspaces delivers further improvements to basic functionality with a smoother taskbar, smarter battery widget and improved sound mixer. The introduction of KScreen brings intelligent multi-monitor handling to the Workspaces, and large scale performance improvements combined with small usability tweaks make for an overall nicer experience.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

This release marks massive improvements in the KDE PIM stack, giving much better performance and many new features. Kate improves the productivity of Python and Javascript developers with new plugins, Dolphin became faster and the educational applications bring various new features.




