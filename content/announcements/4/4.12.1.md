---
aliases:
- ../announce-4.12.1
date: '2014-01-14'
description: KDE Ships Applications and Platform 4.12.1.
title: KDE Ships January Updates to Applications and Platform
---

January 14, 2014. Today KDE released updates for its Applications and Development Platform, the first in a series of monthly stabilization updates to the 4.12 series. Starting with the 4.12.2 releases, the KDE Workspaces 4.11.x releases will be synchronized with those of KDE Applications and Development Platform 4.12.x. This release contains only bugfixes and translation updates; it will be a safe and pleasant update for everyone.
<br /><br />
More than 45 recorded bugfixes include improvements to the personal information management suite Kontact, the UML tool Umbrello, the document viewer Okular, the web browser Konqueror, the file manager Dolphin, and others. Umbrello standardizes the global, diagram and widget settings and adds a clone diagram function. Dolphin gets a bug fixed that slowed down pdf preview under certain circumstances. In Kontact, several bugs and regressions were fixed in the KOrganizer, Akregator and KMail components.
<br /><br />

{{% i18n_var "A more complete <a href='%[1]s'>list</a> of changes can be found in KDE's issue tracker. Browse the Git logs for a detailed list of changes in 4.12.1." "https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.12.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=" %}}
<br /><br />
To download source code or packages to install go to the <a href='http://www.kde.org/info/4.12.1.php'>4.12.1 Info Page</a>. If you want to find out more about the 4.12 versions of KDE Applications and Development Platform, please refer to the <a href='http://www.kde.org/announcements/4.12/'>4.12 release notes</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a
href='http://download.kde.org/stable/4.12.1/'>http://download.kde.org</a> or from any of the <a href='http://www.kde.org/download/distributions.php'>major GNU/Linux and UNIX systems</a> shipping today.

#### Installing 4.12.1 Binary Packages

<em>Packages</em>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.12.1 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>.
For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='/info/4.12.1.php#binary'>4.12.1 Info Page</a>.

#### Compiling 4.12.1

The complete source code for 4.12.1 may be <a href='http://download.kde.org/stable/4.12.1/src/'>freely downloaded</a>. Instructions on compiling and installing 4.12.1 are available from the <a href='/info/4.12.1.php'>4.12.1 Info Page</a>.

#### Supporting KDE

KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='http://jointhegame.kde.org/'>Join the Game</a> initiative.



