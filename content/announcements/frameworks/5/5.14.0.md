---
aliases:
- ../../kde-frameworks-5.14.0
date: 2015-09-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### In many frameworks

- Rename private classes to avoid exporting them accidentally

### Baloo

- Add org.kde.baloo interface to root object for backward compatibility
- Install a fake org.kde.baloo.file.indexer.xml to fix compilation of plasma-desktop 5.4
- Re-organize D-Bus interfaces
- Use json metadata in kded plugin and fix plugin name
- Create one Database instance per process (bug 350247)
- Prevent baloo_file_extractor being killed while committing
- Generate xml interface file using qt5_generate_dbus_interface
- Baloo monitor fixes
- Move file url export to main thread
- Make sure cascaded configs are taken into account
- Do not install namelink for private library
- Install translations, spotted by Hrvoje Senjan.

### BluezQt

- Don't forward deviceChanged signal after device was removed (bug 351051)
- Respect -DBUILD_TESTING=OFF

### Extra CMake Modules

- Add macro to generate logging category declarations for Qt5.
- ecm_generate_headers: Add COMMON_HEADER option and multiple header functionality
- Add -pedantic for KF5 code (when using gcc or clang)
- KDEFrameworkCompilerSettings: only enable strict iterators in debug mode
- Also set the default visibility for C code to hidden.

### Framework Integration

- Also propagate window titles for folder-only file dialogs.

### KActivities

- Only spawn one action loader (thread) when the actions of the FileItemLinkingPlugin are not initialized (bug 351585)
- Fixing the build problems introduced by renaming the Private classes (11030ffc0)
- Add missing boost include path to build on OS X
- Setting the shortcuts moved to activity settings
- Setting the private activity mode works
- Refactor of the settings UI
- Basic activity methods are functional
- UI for the activity configuration and deletion pop-ups
- Basic UI for the activities creation/deletion/configuration section in KCM
- Increased the chunk size for loading the results
- Added missing include for std::set

### KDE Doxygen Tools

- Windows fix: remove existing files before we replace them with os.rename.
- Use native paths when calling python to fix Windows builds

### KCompletion

- Fix bad behavior / running OOM on Windows (bug 345860)

### KConfig

- Optimize readEntryGui
- Avoid QString::fromLatin1() in generated code
- Minimize calls to expensive QStandardPaths::locateAll()
- Finish the port to QCommandLineParser (it has addPositionalArgument now)

### KDELibs 4 Support

- Port solid-networkstatus kded plugin to json metadata
- KPixmapCache: create dir if it doesn't exist

### KDocTools

- Sync Catalan user.entities with English (en) version.
- Add entities for sebas and plasma-pa

### KEmoticons

- Performance: cache a KEmoticons instance here, not a KEmoticonsTheme.

### KFileMetaData

- PlainTextExtractor: enable O_NOATIME branch on GNU libc platforms
- PlainTextExtractor: make the Linux branch work also without O_NOATIME
- PlainTextExtractor: fix error check on open(O_NOATIME) failure

### KGlobalAccel

- Only start kglobalaccel5 if needed.

### KI18n

- Gracefully handle no newline at end of pmap file

### KIconThemes

- KIconLoader: fix reconfigure() forgetting about inherited themes and app dirs
- Adhere better to the icon loading spec

### KImageFormats

- eps: fix includes related to Qt Caterogized Logging

### KIO

- Use Q_OS_WIN instead of Q_OS_WINDOWS
- Make KDE_FORK_SLAVES work under Windows
- Disable installation of desktop file for ProxyScout kded module
- Provide deterministic sort order for KDirSortFilterProxyModelPrivate::compare
- Show custom folder icons again (bug 350612)
- Move kpasswdserver from kded to kiod
- Fix porting bugs in kpasswdserver
- Remove legacy code for talking very very old versions of kpasswdserver.
- KDirListerTest: use QTRY_COMPARE on both statements, to fix race showed by CI
- KFilePlacesModel: implement old TODO about using trashrc instead of a full-blown KDirLister.

### KItemModels

- New proxymodel: KConcatenateRowsProxyModel
- KConcatenateRowsProxyModelPrivate: fix handling of layoutChanged.
- More checking on the selection after sorting.
- KExtraColumnsProxyModel: fix bug in sibling() which broke e.g. selections

### Package Framework

- kpackagetool can uninstall a package from a package file
- kpackagetool is now smarter about finding the right servicetype

### KService

- KSycoca: check timestamps and run kbuildsycoca if needed. No kded dependency anymore.
- Don't close ksycoca right after opening it.
- KPluginInfo now correctly handles FormFactor metadata

### KTextEditor

- Merge allocation of TextLineData and ref count block.
- Change default keyboard shortcut for "go to previous editing line"
- Syntax highlighting Haskell comment fixes
- Speed up code-completion pop-up appearance
- minimap: Attempt to improve the look and feel (bug 309553)
- nested comments in Haskell syntax highlighting
- Fix problem with wrong unindent for python (bug 351190)

### KWidgetsAddons

- KPasswordDialog: let the user change the password visibility (bug 224686)

### KXMLGUI

- Fix KSwitchLanguageDialog not showing most languages

### KXmlRpcClient

- Avoid QLatin1String wherever it allocates heap memory

### ModemManagerQt

- Fix metatype conflict with the latest nm-qt change

### NetworkManagerQt

- Added new properties from the latest NM snapshot/releases

### Plasma Framework

- reparent to flickable if possible
- fix package listing
- plasma: Fix applet actions might be nullptr (bug 351777)
- The onClicked signal of PlasmaComponents.ModelContextMenu now works properly
- PlasmaComponents ModelContextMenu can now create Menu sections
- Port platformstatus kded plugin to json metadata...
- Handle an invalid metadata in PluginLoader
- Let the RowLayout figure out the size of the label
- always show the edit menu when the cursor is visible
- Fix loop on ButtonStyle
- Don't change the flat-iness of a button on pressed
- on touchscreen and mobile scrollbars are transient
- adjust flick velocity&amp;deceleration to dpi
- custom cursor delegate only if mobile
- touch friendly text cursor
- fix parenting and popping up policy
- declare __editMenu
- add missing cursot handles delegates
- rewrite the EditMenu implementation
- use the mobile menu only conditionally
- reparent the menu to root

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
