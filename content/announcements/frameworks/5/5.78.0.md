---
aliases:
- ../../kde-frameworks-5.78.0
date: 2021-01-09
layout: framework
qtversion: 5.14
libCount: 83
---

### Attica

* Honour the job being aborted immediately (bug 429939)

### Baloo

* [ExtractorProcess] Move DBus signal from helper to main process
* [timeline] Consolidate code for root folder stat and list
* Make toplevel ioslave UDS entries readonly
* Avoid errors for application startup if no baloo index was ever created
* [BasicIndexingJob] Strip trailing slash from folders (bug 430273)

### Breeze Icons

* New compass action icon
* Add image-missing icon to theme
* Add icon for WIM images

### Extra CMake Modules

* Tell MSVC that our source files are UTF-8 encoded
* Add Findepoxy.cmake
* Consider local fastlane image assets
* Reproducible tarballs with GNU tar only
* Preserve the rich-text subset supported by F-Droid
* Bump required cmake version for Android.cmake (bug 424392)
* Automatically detect plugin lib deps on Android
* Check if file exists before removing the fastlane archive
* Clean image folder and archive file before downloading/generating those
* Retain screenshot order from the appstream file
* Windows: fix QT_PLUGIN_PATH for tests
* Don't fail if we haven't found any categories
* Make KDEPackageAppTemplates to create reproducible tarball

### KActivitiesStats

* Remove broken lastQuery feature, fixes krunner crashes for me

### KCalendarCore

* CMakeLists.txt - increase minimum libical version to 3.0

### KCMUtils

* KPluginSelector implement default highlight indicator
* kcmoduleqml: don't bind column width to view width (bug 428727)

### KCompletion

* [KComboBox] fix crash when calling setEditable(false) with open context menu

### KConfig

* Fix windows being inappropriately maximized on launch (bug 426813)
* Correct format of window maximized string
* Fix window sizing and positioning on Windows (bug 429943)

### KConfigWidgets

* KCodecAction: add non-overload signals codecTriggered & encodingProberTriggered

### KCoreAddons

* Port KJobTrackerInterface to Qt5 connect syntax
* KTextToHtml: fix assert due to out of bounds at() call
* Use flat hierarchy for plugin paths on Android
* desktop to JSON conversion: ignore "Actions=" entry
* Deprecate KProcess::pid()
* ktexttohtml: fix KTextToHTMLHelper usage

### KCrash

* Use std::unique_ptr<char[]> to prevent memory leaks

### KDeclarative

* Switch to Findepoxy provided by ECM
* KCMShell: Add support for passing arguments
* Workaround crash with GL detection and kwin_wayland
* [KQuickAddons] QtQuickSettings::checkBackend() for fallback to software backend (bug 346519)
* [abstractkcm] Fix import version in code example
* Avoid setting QSG_RENDER_LOOP if set already
* ConfigPropertyMap : load property's default value in the map

### KDocTools

* Add an entity for MathML acronym
* Change 'Naval Battle' to 'KNavalBattle' to meet legal matters

### KGlobalAccel

* Avoid autostarting kglobalaccel when shutting down (bug 429415)

### KHolidays #

* Update Japanese holidays

### KIconThemes

* Skip warning for some Adwaita icons for backward compatibility
* QSvgRenderer::setAspectRatioMode() was introduced in Qt 5.15

### KImageFormats

* Add AVIF to the list of supported formats
* Add plugin for AV1 Image File Format (AVIF)

### KIO

* [KFileItemDelegate] do not waste space for non-existing icons in columns other than first
* KFilePlacesView, KDirOperator: port to async askUserDelete()
* Rework the way CopyJob finds the JobUiDelegate extensions
* Introduce AskUserActionInterface, an async API for Rename/Skip dialogs
* RenameDialog: only call compareFiles() on files
* kcm/webshortcuts: Fix Reset button
* KUrlNavigatorMenu: fix middle-click handling
* Remove knetattach item from the remote:// ioslave's view (bug 430211)
* CopyJob: port to AskUserActionInterface
* Jobs: add non-overloaded signal "mimeTypeFound" to deprecate "mimetype"
* RenameDialog: Add missing nullptr initialization (bug 430374)
* KShortUriFilter: don't filter "../" and co. strings
* Do not assert if KIO::rawErrorDetail() is given a URL with no scheme (bug 393496)
* KFileItemActions: fix condition, we want to exclude only remote dirs (bug 430293)
* KUrlNavigator: remove kurisearchfilter usage
* KUrlNavigator: make completions of relative paths work (bug 319700)
* KUrlNavigator: resolve relative dir paths (bug 319700)
* Silence warnings due to samba config issues when not using samba explicitly
* KFileWidget: allow files that begin with a ':' to be selected (bug 322837)
* [KFileWidget] fixed bookmark button position in toolbar
* KDirOperator: deprecate mkdir(const QString &, bool)
* KFilePlacesView: allow setting a static icon size (bug 182089)
* KFileItemActions: add new method to insert openwith actions (bug 423765)

### Kirigami

* [controls/SwipeListItem]: Always show actions on desktop by default
* [overlaysheet] Use more conditional positioning for close button (bug 430581)
* [controls/avatar]: Open up internal AvatarPrivate as public NameUtils API
* [controls/avatar]: expose generated colour
* Add Hero component
* [controls/Card]: Remove hover animation
* [controls/ListItem]: Remove hover animation
* Move ListItems to use veryShortDuration for hover instead of longDuration
* [controls/Units]: Add veryShortDuration
* ActionButton icon coloring
* use icon pixmaps only as big as needed
* [controls/avatar]: better default appearance
* [controls/avatar]: Fix visual bugs
* Create CheckableListItem component
* [controls/avatar]: scale border according to avatar size
* Revert "[Avatar] Change background gradient"
* Revert "[Avatar] Change border width to 1px to match other bother widths"
* [controls/avatar]: Make Avatar accessibility-friendly
* [controls/avatar]: Increase padding of icon fallback
* [controls/avatar]: Make image mode set sourceSize
* [controls/avatar]: Adjust sizing of text
* [controls/avatar]: Adjust techniques used for circular shape
* [controls/avatar]: Add primary/secondary action to Avatar
* Hardcode OverlaySheet header item padding
* qmake build: add missing sizegroup source/header
* Color icons, not buttons (bug 429972)
* Fix header back and forward buttons having no width
* [BannerImage]: fix not vertically centred header title with non-plasma themes

### KItemModels

* Add count property, allows rowCount binding in QML

### KItemViews

* KWidgetItemDelegate allow to trigger a resetModel from KPluginSelector

### KNewStuff

* Deprecate standardAction and standardActionUpload methods
* Fix QtQuick model if there is only a payload, but no download links
* Add a dptr to Cache, and move the throttle timer there to fix crash (bug 429442)
* Refactor KNS3::Button to use new dialog internally
* Create wrapper class for QML dialog
* Check if version is empty before concatenating version

### KNotification

* Improve KNotification API docs

### KParts

* Deprecate BrowserHostExtension

### KQuickCharts

* Use a custom macro for deprecation messages in QML
* Use ECMGenerateExportHeader for deprecation macros and use them
* Changing interval does not need to clear history
* Change continuous line chart to history proxy source example
* Deprecate Model/ValueHistorySource
* Introduce HistoryProxySource as a replacement for Model/ValueHistorySource
* Add logging categories for charts and use them for existing warnings

### KRunner

* [DBus Runner] Add support for custom pixmap icons for results
* Add key to check if the config was migrated
* Separate config and data files
* New API to run matches and for history
* Do not build RunnerContextTest on windows

### KService

* KSycoca: avoid database rebuild if XDG_CONFIG_DIRS contains duplicates
* KSycoca: ensure extrafiles are ordered for comparison (bug 429593)

### KTextEditor

* Rename "Variable:" to "Document:Variable:"
* Variable Expansion: Fix finding prefix matches with multiple colons
* Move painting from KateTextPreview into KateRenderer
* Make sure only lines in view are used to paint the pixmap
* Use KateTextPreview to render the pixmap
* Variable expansion: Add support for %{Document:Variable:<name>}
* Show the dragged text when dragging (bug 398719)
* Fix detach in TextRange::fixLookup()
* Don't paint currentLine bg if there is an overlapping selection
* KateRegExpSearch: fix logic when adding '\n' between range lines
* rename action to 'Swap with clipboard contents'
* add an action to trigger copy & paste as one action
* feat: add text-wrap action icon for Dynamic Word Wrap
* Undo indent in one step (bug 373009)

### KWidgetsAddons

* KSelectAction: add non-overload signals indexTriggered & textTriggered
* KFontChooserDialog: handle dialog being deleted by parent during exec()
* KMessageDialog: call setFocus() on the default button
* Port from QLocale::Norwegian to QLocale::NorwegianBokmal
* Port KToolBarPopupActionTest to QToolButton::ToolButtonPopupMode

### KXMLGUI

* KXmlGui: when upgrading a local .rc file, keep new app toolbars
* Fix key recording by setWindow before capture starts (bug 430388)
* Remove unused KWindowSystem dependency
* Clear KXMLGUIClient in memory xml doc after saving shortcuts to disk

### Oxygen Icons

* Add upindicator

### Plasma Framework

* Expose error information to error plasmoid in a more structured manner
* [components] Hook up Mnemonics
* [svg] Always start SvgRectsCache timer from the correct thread
* [PC3 ProgressBar] Set binding for width (bug 430544)
* fix Windows build + inversion of variables
* [PlasmaComponents/TabGroup] Fix check if item inherits from Page
* Port various components to veryShortDuration on hover
* Move ListItems to use veryShortDuration for hover instead of longDuration
* Add veryShortDuration
* Don't allow negative calendar years (bug 430320)
* Fix broken background (bug 430390)
* Replace QString cache IDs with a struct-based version
* [TabGroup] Reverse animations in RTL mode
* Only remove shortcuts on applet removal not destruction
* Hide disabled contextual actions from ExpandableListItem

### Purpose

* KFileItemActions: add menu windowflag
* Share fileitemplugin: use parent Widget as menu parent (bug 425997)

### QQC2StyleBridge

* Update org.kde.desktop/Dialog.qml
* Draw ScrollView using Frame instead of Edit (bug 429601)

### Sonnet

* Improve performance of createOrderedModel using QVector
* Avoid runtime warning if no guess result exists

### Syntax Highlighting

* C++ Highlighting: QOverload and co
* Fix labels beginning with period not being highlighted in GAS
* C++ highlighting: add qGuiApp macro
* Improve dracula theme
* fix #5: Bash, Zsh: ! with if, while, until ; Bash: pattern style for ${var,patt} and ${var^patt}
* fix #5: Bash, Zsh: comments within array
* Cucumber feature syntax
* Zsh: increment syntax version
* Zsh: fix brace expansion in a command
* add weakDeliminator and additionalDeliminator with keyword, WordDetect, Int, Float, HlCOct and HlCHex
* Indexer: reset currentKeywords and currentContext when opening a new definition
* Zsh: many fixes and improvements
* Bash: fix comments in case, sequence expression and ) after ]
* Handle import color in base correctly and specialize for c/c++
* Update Monokai theme
* Verify the correctness/presence of custom-styles in themes
* Add GitHub Dark/Light themes
* increment version, don't change kate version, until I understand why it is needed
* Add licenses
* Add Atom One dark/light themes
* missed to increment version on change
* Fix monokai attribute & operator color
* Add Monokai theme
* CMake: Add missed 3.19 variables and some new added in 3.19.2
* Kotlin: fix some issues and other improvements
* Groovy: fix some issues and other improvements
* Scala: fix some issues and other improvements
* Java: fix some issues and other improvements
* fix && and || in a subcontext and fix function name pattern
* add QRegularExpression::DontCaptureOption when there is no dynamic rule
* Bash: add (...), ||, && in [[ ... ]] ; add backquote in [ ... ] and [[ ... ]]

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
