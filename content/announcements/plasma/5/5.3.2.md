---
aliases:
- ../../plasma-5.3.2
changelog: 5.3.1-5.3.2
date: 2015-06-30
layout: plasma
figure:
  src: /announcements/plasma/5/5.3.0/plasma-5.3.png
---

{{< i18n_date >}}. {{< i18n msgname="annc-plasma-bugfix-intro" maj="5" version="5.3.2" >}}

{{% i18n msgname="annc-plasma-bugfix-minor-release-4" maj="5" maj_min="5.3" year="2015 " %}}

{{< i18n "annc-plasma-bugfix-worth-5" >}} {{< i18n "annc-plasma-bugfix-last" >}}

- KWin: 'Defaults' should set the title bar double-click action to 'Maximize.'. <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=26ee92cd678b1e070a3bdebce100e38f74c921da">Commit.</a>
- Improve Applet Alternatives dialog. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0e90ea5fea7947acaf56689df18bbdce14e8a35f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/345786">#345786</a>
- Make shutdown scripts work. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=96fdec6734087e54c5eee7c073b6328b6d602b8e">Commit.</a>
