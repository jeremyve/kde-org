---
aliases:
- ../../plasma-5.4.1
changelog: 5.4.0-5.4.1
date: 2015-09-08
layout: plasma
figure:
  src: /announcements/plasma/5/5.4.0/plasma-screen-desktop-2-shadow.png
---

{{< i18n_date >}}. {{< i18n msgname="annc-plasma-bugfix-intro" maj="5" version="5.4.1" >}}

{{% i18n msgname="annc-plasma-bugfix-minor-release-8" maj="5" maj_min="5.4" year="2015 " %}}

{{< i18n "annc-plasma-bugfix-worth-5" >}} {{< i18n "annc-plasma-bugfix-last" >}}

- Fixes for compilation with GCC 5
- Autostart desktop files no longer saved to the wrong location
- On Muon Make sure the install button has a size.
