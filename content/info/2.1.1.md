---
title : "KDE 2.1.1 Info Page"
publishDate: 2001-03-27 00:01:00
unmaintained: true
---

<p>
KDE 2.1.1 was released on March 27, 2001. Read the
<a href="/announcements/announce-2.1.1.php">official announcement</a>.
</p>

<p>For a list of changes since KDE 2.1, see the
<a href="/announcements/changelogs/changelog2_1to2_1_1.php">list of changes</a></p>

<p>For a high-level overview of the features of KDE, see the
<a href="/info">KDE info page</a></p>

<p>For a graphical tutorial on using KDE 2, see this
<a href="http://www.linux-mandrake.com/en/demos/Tutorial/">tutorial page</a> from Linux Mandrake</p>

<p>This page will be updated to reflect changes in the status of
2.1.1 so check back for new information.</p>

<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed to the
<a href="http://konqueror.kde.org/faq/">Konqueror FAQ</a> and sound related
questions are answered in the <a href="http://www.arts-project.org/doc/handbook/faq.html">Arts FAQ</a>

<h2>Download and Installation</h2>

See the links listed in the <a
href="/announcements/announce-2.1.1.php">announcement</a>. The KDE
<a href="/documentation/faq/install.html">Installation FAQ</a>
provides generic instruction about installation issues.

<p>If you want to compile from sources we offer
<a href="http://developer.kde.org/build/">instructions</a> and help for common
problems in the <a href="http://developer.kde.org/build/compilationfaq.html">Compilation
FAQ</a>.</p>

<h2>Updates</h2>

<p><a href="../announcements/announce-2.1.2.php">kdelibs 2.1.2</a> have been released on April 30, 2001.</p>

<h2>Security Issues</h2>

<p><b>NOTE:</b>This section is no longer maintained. Please refer to the
<a href="2.2.2.php">2.2.2 Info page</a> instead.</p>

<p>A security problem in KDEsu was fixed in the
<a href="../announcements/announce-2.1.2.php">kdelibs 2.1.2</a> update.</p>

<h2>Bugs</h2>

Here we will provide a list of grave bugs or common pitfalls
surfacing after the release date. 

<p>Please check the bug <a href="http://bugs.kde.org">database</a>
before filing any bug reports. Also check for possible updates that
might fix your problem.</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 2.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/2.2/kdelibs/KDE2PORTING.html">
porting guide</a> or discuss your problems with fellow developers
on the <a href="http://mail.kde.org/mailman/listinfo/kde2-porting">kde&#x32;-portin&#x67;&#x40;&#107;de.org</a>
mailing list. 

<p>There is also info on the <a
href="http://developer.kde.org/documentation/kde2arch/index.html">architecture</a>
and the <a
href="http://developer.kde.org/documentation/library/2.1-api/classref/index.html">
programming interface of KDE 2.1</a>.</p>
