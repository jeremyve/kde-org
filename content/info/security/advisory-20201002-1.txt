KDE Project Security Advisory
=============================

Title:           KDE Connect: packet manipulation can be exploited in a Denial of Service attack
Risk Rating:     Important
CVE:             CVE-2020-26164
Versions:        kdeconnect <= 20.08.1
Author:          Albert Vaca Cintora <albertvaka@gmail.com>
Date:            2 October 2020

Overview
========

An attacker on your local network could send maliciously crafted packets to other hosts running
kdeconnect on the network, causing them to use large amounts of CPU, memory or network
connections, which could be used in a Denial of Service attack within the network.

Impact
======

Computers that run kdeconnect are susceptible to DoS attacks from the local network.

Workaround
==========

We advise you to stop KDE Connect when on untrusted networks like those on airports or conferences.

Since kdeconnect is dbus activated it is relatively hard to make sure it stays stopped so the brute
force approach is to uninstall the kdeconnect package from your system and then run
    kquitapp5 kdeconnectd
Just install the package again once you're back in a trusted network.

Solution
========

KDE Connect 20.08.2 patches several code paths that could result in a DoS.
You can apply these patches on top of 20.08.1:
https://invent.kde.org/network/kdeconnect-kde/-/commit/f183b5447bad47655c21af87214579f03bf3a163
https://invent.kde.org/network/kdeconnect-kde/-/commit/b279c52101d3f7cc30a26086d58de0b5f1c547fa
https://invent.kde.org/network/kdeconnect-kde/-/commit/d35b88c1b25fe13715f9170f18674d476ca9acdc
https://invent.kde.org/network/kdeconnect-kde/-/commit/b496e66899e5bc9547b6537a7f44ab44dd0aaf38
https://invent.kde.org/network/kdeconnect-kde/-/commit/5310eae85dbdf92fba30375238a2481f2e34943e
https://invent.kde.org/network/kdeconnect-kde/-/commit/721ba9faafb79aac73973410ee1dd3624ded97a5
https://invent.kde.org/network/kdeconnect-kde/-/commit/ae58b9dec49c809b85b5404cee17946116f8a706
https://invent.kde.org/network/kdeconnect-kde/-/commit/66c768aa9e7fba30b119c8b801efd49ed1270b0a
https://invent.kde.org/network/kdeconnect-kde/-/commit/85b691e40f525e22ca5cc4ebe79c361d71d7dc05
https://invent.kde.org/network/kdeconnect-kde/-/commit/48180b46552d40729a36b7431e97bbe2b5379306

Credits
=======

Thanks Matthias Gerstner and the openSUSE security team for reporting the issue.
Thanks to Aleix Pol, Nicolas Fella and Albert Vaca Cintora for the patches.
