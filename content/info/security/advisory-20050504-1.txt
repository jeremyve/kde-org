-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: Updated patches for Advisories 20050420/21
Original Release Date: 2005-05-04
URL: http://www.kde.org/info/security/advisory-20050504-1.txt

0. References

        http://www.kde.org/info/security/advisory-20050421-1.txt
        http://www.kde.org/info/security/advisory-20050420-1.txt

1. Systems affected:

        All KDE versions patched according to the KDE security
        advisories 20050421-1 and 20050420-1.


2. Overview:

        The patches released by the previous KDE security updates
        were faulty and introduced regressions. The updated
        patches below correct the introduced problems.

        The Kimgio patch broke reading of .rgb images in
        most cases due to a fence-post error. 

        The Kommander patch was incorrect and still
        allowed execution of files served from /tmp.

5. Patch:

        A patch for KDE 3.4.0 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

e6398f3326d782d2c9b1543fab683b43  post-3.4.0-kdelibs-kimgio-fixed.diff
80a64324816d321934feeb6638050e4f  post-3.4.0-kdewebdev-kommander-fixed.diff

        A patch for KDE 3.3.2 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

acc1153e58b45f92b43747dbc6693e41  post-3.3.2-kdelibs-kimgio-fixed.diff
13a20ddfff937f77e74cdfad6436ba72  post-3.3.2-kdewebdev-kommander-fixed.diff


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.0 (GNU/Linux)

iD8DBQFCerqgvsXr+iuy1UoRAjgGAJ9sFk8lmkgFgs3OtznWCgwhdjqq7wCgmij9
aCsPESUYxBWBcXYv6/QbBbw=
=KqGZ
-----END PGP SIGNATURE-----
