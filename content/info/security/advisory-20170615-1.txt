KDE Project Security Advisory
=============================

Title:          KMail: Send Later with Delay bypasses OpenPGP
Risk Rating:    Medium
CVE:            CVE-2017-9604
Versions:       kmail, messagelib < 5.5.2
Date:           15 June 2017


Overview
========
KMail’s Send Later with Delay function bypasses OpenPGP signing and
encryption, causing the message to be sent unsigned and in plain-text.

Solution
========
Update to kmail, messagelib >= 5.5.2 (Released as part of KDE Applications 17.04.2)

Or apply the following patches:
     kmail: https://commits.kde.org/kmail/78c5552be2f00a4ac25bd77ca39386522fca70a8
messagelib: https://commits.kde.org/messagelib/c54706e990bbd6498e7b1597ec7900bc809e8197

Credits
=======
Thanks to Daniel Aleksandersen for the report and to Laurent Montel for the fix.
