
KDE Security Advisory: Konqueror address bar spoofing
Original Release Date: 2007-09-14
URL: http://www.kde.org/info/security/advisory-20070914-1.txt

0. References
         CVE-2007-4224
         CVE-2007-4225
         CVE-2007-3820

1. Systems affected:

        Konqueror as shipped with KDE up to including KDE 3.5.7.
        This advisory replaces the KDE advisory 20070816-1.


2. Overview:

        The Konqueror address bar is vulnerable to spoofing attacks
        that are based on embedding white spaces in the url. In addition
        the address bar could be tricked to show an URL which it is
        intending to visit for a short amount of time instead of the
        current URL.


3. Impact:

        Malicious web sites could spoof another website's URL. The
        attack is limited to the address bar, it does not affect
        additional security measures, like for example the SSL certificate
        validation.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patches for KDE 3.5.7 and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        e15d6b5580c5a20ab935f8e553d113e0  post-3.5.7-kdebase-konqueror-2.diff
        4c0fb2576875ded606f276421fc49752  post-3.5.7-kdelibs-kdecore-2.diff

        Patches for KDE 3.4.2 and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        d9a07e8d9a138ef9da90b7af8e35d977  post-3.4.2-kdebase-konqueror.diff
