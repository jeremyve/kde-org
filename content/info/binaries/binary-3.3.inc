<ul>
<!-- CONECTIVA LINUX -->
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>:
  <ul type="disc">
    <li>
      CL10:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/Conectiva/cl10/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!--   FREEBSD -->
<li><a href="http://www.freebsd.org/">FreeBSD</a>
  (<a href="http://download.kde.org/stable/3.3/FreeBSD/README">README</a>)
  <p />
</li>

<!-- KDE-RedHat -->
<li>
 <a href="http://kde-redhat.sourceforge.net/">KDE-RedHat (unofficial) Packages</a>:
 <ul type="disc">
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/7.3/">Red Hat 7.3</a>
 </li>
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/8.0/">Red Hat 8.0</a>
 </li>
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/9/">Red Hat 9</a>
 </li>
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/1/">Fedora Core 1</a>
 </li>
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/2/">Fedora Core 2</a>
 </li>
 <li>
 <a href="ftp://apt.kde-redhat.org/apt/kde-redhat/3.0/">Red Hat Enterprise 3</a>
 </li>
 </ul>
 <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/contrib/Slackware/10.0/README">README</a>)
   :
   <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/contrib/Slackware/noarch/">Language packages</a>
    </li>
     <li>
       10.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/contrib/Slackware/10.0/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!-- SOLARIS -->
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/contrib/Solaris/9/I18N/">Language
        packages</a> (all versions)
    </li>
    <li>
      8: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/contrib/Solaris/8/">Sparc</a>
    </li>
    <li>
      9: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/contrib/Solaris/9/">Sparc</a>
    </li>
  </ul>
  <p />
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
<!--  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/SuSE/README">README</a>) -->
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/SuSE/ix86/9.1/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/SuSE/ix86/9.0/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/SuSE/x86_64/9.0/">AMD x86-64</a>
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/SuSE/ix86/8.2/">Intel i586</a>
    </li>
    <li>
      8.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/SuSE/ix86/8.2/">Intel i586</a>
    </li>
  </ul>
  <p />
</li>

<!-- YOPER LINUX -->
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      V2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3/Yoper/">Intel i686 rpm</a>
    </li>
  </ul>
  <p />
</li>

</ul>
