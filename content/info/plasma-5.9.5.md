---
version: "5.9.5"
title: "KDE Plasma 5.9.5, Bugfix Release"
errata:
    link: https://community.kde.org/Plasma/5.9_Errata
    name: 5.9 Errata
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
---

This is a bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.9.5 announcement](/announcements/plasma-5.9.5).
